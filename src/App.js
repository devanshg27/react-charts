import React, { Component } from 'react';
import './App.css';

import FlotController from './controllers/flot.js';
import EasypieController from './controllers/easypie.js';
import ChartJSController from './controllers/chartjs.js';
import RickshawController from './controllers/rickshaw.js';
import Nvd3Controller from './controllers/nvd3.js';
import C3jsController from './controllers/c3js.js';

require('../node_modules/bootstrap/dist/css/bootstrap.css');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {library: 'flot'};

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({library: event.target.value});
  }

  render() {
    return (
      <div className="App row">
        <div className="App-select col-sm-2">
          <select value={this.state.library} onChange={this.handleChange}>
            <option value="flot">flot</option>
            <option value="easypie">easypie</option>
            <option value="chartjs">chartjs</option>
            <option value="rickshaw">rickshaw</option>
            <option value="nvd3">nvd3</option>
            <option value="c3js">c3js</option>
          </select>
        </div>
        <div className="App-viewer col-sm-8">
          {this.state.library === "flot" &&
            <FlotController />
          }
          {this.state.library === "easypie" &&
            <EasypieController />
          }
          {this.state.library === "chartjs" &&
            <ChartJSController />
          }
          {this.state.library === "rickshaw" &&
            <RickshawController />
          }
          {this.state.library === "nvd3" &&
            <Nvd3Controller />
          }
          {this.state.library === "c3js" &&
            <C3jsController />
          }
        </div>
      </div>
    );
  }
}

export default App;
