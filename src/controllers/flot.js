import React, { Component } from 'react';
import $ from 'jquery';
window.jQuery = $;
var ReactFlot = require('react-flot');

require('../../node_modules/react-flot/flot/jquery.flot.resize.min');
require('../../node_modules/react-flot/flot/jquery.flot.categories.min');
require('../../node_modules/react-flot/flot/jquery.flot.stack.min');
require('../../node_modules/react-flot/flot/jquery.flot.time.min');
require('../../node_modules/react-flot/flot/jquery.flot.pie.min');

var COLORS = {
  'default': '#e2e2e2',
  'primary': '#09c',
  'success': '#2ECC71',
  'warning': '#ffc65d',
  'danger': '#d96557',
  'info': '#4cc3d9',
  'white': 'white',
  'dark': '#4C5064',
  'border': '#e4e4e4',
  'bodyBg': '#e0e8f2',
  'textColor': '#6B6B6B'
};

var getRandomArbitrary = function() {
  return Math.round(100 * Math.random())
}

var visits = [[0, getRandomArbitrary()], [1, getRandomArbitrary()], [2, getRandomArbitrary()], [3, getRandomArbitrary()], [4, getRandomArbitrary()], [5, getRandomArbitrary()], [6, getRandomArbitrary()], [7, getRandomArbitrary()], [8, getRandomArbitrary()]];
var visitors = [[0, getRandomArbitrary()], [1, getRandomArbitrary()], [2, getRandomArbitrary()], [3, getRandomArbitrary()], [4, getRandomArbitrary()], [5, getRandomArbitrary()], [6, getRandomArbitrary()], [7, getRandomArbitrary()], [8, getRandomArbitrary()]];


var lineDataset = [{
  data: visits,
  color: COLORS.primary
  }, {
  data: visitors,
  color: COLORS.info
  }];

var lineOptions = {
  series: {
  lines: {
    show: true,
    fill:0.2,
    lineWidth: 0,
  },
  shadowSize: 0
  },
  grid: {
  color: COLORS.border,
  borderWidth: 1,
  hoverable: true,
  }
};

var barDataset = [{
  data: [[1391761856000, 80], [1394181056000, 40], [1396859456000, 20], [1399451456000, 20], [1402129856000, 50]],
  bars: {
    show: true,
    barWidth: 7 * 24 * 60 * 60 * 1000,
    fill: true,
    lineWidth: 0,
    order: 1,
    fillColor: COLORS.info
  }
}, {
  data: [[1391761856000, 50], [1394181056000, 30], [1396859456000, 10], [1399451456000, 70], [1402129856000, 30]],
  bars: {
    show: true,
    barWidth: 7 * 24 * 60 * 60 * 1000,
    fill: true,
    lineWidth: 0,
    order: 2,
    fillColor: COLORS.danger
  }
}, {
  data: [[1391761856000, 30], [1394181056000, 60], [1396859456000, 40], [1399451456000, 40], [1402129856000, 40]],
  bars: {
    show: true,
    barWidth: 7 * 24 * 60 * 60 * 1000,
    fill: true,
    lineWidth: 0,
    order: 3,
    fillColor: COLORS.success
  }
}];
var barOptions = {
  grid: {
    hoverable: false,
    clickable: false,
    labelMargin: 8,
    color: COLORS.border,
    borderWidth: 0,
  },
  xaxis: {
    mode: 'time',
    timeformat: '%b',
    tickSize: [1, 'month'],
    monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    tickLength: 0,
    axisLabel: 'Month',
    axisLabelUseCanvas: true,
    axisLabelFontSizePixels: 12,
    axisLabelFontFamily: 'Roboto',
    axisLabelPadding: 5
  },
  stack: true
};
var pieDataset = [{
  label: 'IE',
  data: 15,
  color: COLORS.danger
}, {
  label: 'Safari',
  data: 14,
  color: COLORS.info
}, {
  label: 'Chrome',
  data: 34,
  color: COLORS.warning
}, {
  label: 'Opera',
  data: 13,
  color: COLORS.bodyBg
}, {
  label: 'Firefox',
  data: 24,
  color: COLORS.dark
}];
var pieOptions = {
  series: {
    pie: {
      show: true,
      innerRadius: 0.5,
      stroke: {
        width: 0
      },
      label: {
        show: false,
      }
    }
  },
  legend: {
    show: true
  },
};
var data = []
  , totalPoints = 300
  , updateInterval = 300;
var getRandomData = function() {
  if (data.length > 0) {
    data = data.slice(1);
  }
  while (data.length < totalPoints) {
    var prev = data.length > 0 ? data[data.length - 1] : 50
      , y = prev + Math.random() * 10 - 5;
    if (y < 0) {
      y = 0;
    } else if (y > 100) {
      y = 100;
    }
    data.push(y);
  }
  var res = [];
  for (var i = 0; i < data.length; ++i) {
    res.push([i, data[i]]);
  }
  return res;
}
;
var realDataset = [getRandomData()];
setInterval(function() {
  realDataset = [getRandomData()];
}, updateInterval);
var realOptions = {
  colors: [COLORS.dark],
  lines: {
    lineWidth: 1,
  },
  series: {
    shadowSize: 0
  },
  grid: {
    color: COLORS.border,
    borderWidth: 0,
    hoverable: true
  },
  xaxis: {
    show: false
  },
  yaxis: {
    min: 0,
    max: 100
  }
};

class FlotChart extends Component {
  constructor(props) {
    super(props);
    this.state = {realDataset: [getRandomData()]};
  }
  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      realDataset: [getRandomData()]
    });
  }
  render() {
    return (
      <div>
      <div className="row">
        <div className="col-sm-12">
        <div className="panel">
          <div className="panel-heading border"> Line Chart </div>
          <div className="panel-body">
            <ReactFlot id="line-product-chart" options={lineOptions} data={lineDataset} />
          </div>
        </div>
        </div>
      </div>
      <div className="row">
        <div className="col-sm-12">
        <div className="panel">
          <div className="panel-heading border"> Grouped Bar Chart </div>
          <div className="panel-body">
            <ReactFlot id="bar-product-chart" options={barOptions} data={barDataset} />
          </div>
        </div>
        </div>
        <div className="col-sm-6">
        <div className="panel">
          <div className="panel-heading border"> Pie Chart </div>
          <div className="panel-body">
            <ReactFlot id="pie-product-chart" options={pieOptions} data={pieDataset} />
          </div>
        </div>
        </div>
        <div className="col-sm-6">
        <div className="panel">
          <div className="panel-heading border"> Realtime Chart </div>
          <div className="panel-body">
            <ReactFlot id="real-product-chart" options={realOptions} data={realDataset} />
          </div>
        </div>
        </div>
      </div>
      </div>
    );
  }
}

export default FlotChart;