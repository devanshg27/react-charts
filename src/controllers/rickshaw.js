import React, { Component } from 'react';
import Rickshaw from 'rickshaw';
import 'rickshaw/rickshaw.min.css';
import './rickshaw.css';
import $ from 'jquery';
import rickshawdata from './rickshawdata.js';

class RickshawChart extends Component {
  componentDidMount() {
    var graph1 = new Rickshaw.Graph( {
      element: document.querySelector("#rickshaw_chart1"), 
      renderer: 'area',
      series: rickshawdata.series1
    });

    var hoverDetail1 = new Rickshaw.Graph.HoverDetail( {
      graph: graph1,
      xFormatter: function(x) {
        return new Date(x * 1000).toString();
      },
      yFormatter: function(y) {
        return Math.round(y);
      }
    } );

    var axes1 = new Rickshaw.Graph.Axis.Time( {
      graph: graph1
    } );

    var graph2 = new Rickshaw.Graph( {
      element: document.querySelector("#rickshaw_chart2"),
      renderer: 'area',
      series: rickshawdata.series2
    });

    var hoverDetail2 = new Rickshaw.Graph.HoverDetail( {
      graph: graph2,
      xFormatter: function(x) {
        return new Date(x * 1000).toString();
      },
      yFormatter: function(y) {
        return Math.round(y);
      }
    } );

    var axes2 = new Rickshaw.Graph.Axis.Time( {
      graph: graph2
    } );

    var y_ticks2 = new Rickshaw.Graph.Axis.Y( {
      graph: graph2,
      tickFormat: Rickshaw.Fixtures.Number.formatKMBT,
      element: document.getElementById('y_axis')
    } );

    graph1.render();
    graph2.render();
  }

  componentWillUnmount() {
    $('#rickshaw_chart1').empty();
    $('#rickshaw_chart2').empty();
  }

  render() {
    return (
      <div>
        <div className="row mb25">
          <div className="col-xs-12">
            <div className="panel">
              <div className="panel-heading border"> Time on the X-Axis </div>
              <div className="panel-body">
                <div id="rickshaw_chart1" />
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <div className="panel">
              <div className="panel-heading border"> X-Axis and Y-Axis </div>
              <div className="panel-body">
                <div className="chart_container">
                  <div className="y_axis" id="y_axis"></div>
                  <div id="rickshaw_chart2" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RickshawChart;
