import React, { Component } from 'react';
import d3 from 'd3';
import $ from 'jquery';
import nv from 'nvd3';
import 'nvd3/build/nv.d3.min.css';

var COLORS = {
  'default': '#e2e2e2',
  'primary': '#09c',
  'success': '#2ECC71',
  'warning': '#ffc65d',
  'danger': '#d96557',
  'info': '#4cc3d9',
  'white': 'white',
  'dark': '#4C5064',
  'border': '#e4e4e4',
  'bodyBg': '#e0e8f2',
  'textColor': '#6B6B6B'
};

var options1 = {
  chart: {
    height: 150,
    margin: {
      top: 20,
      right: 20,
      bottom: 40,
      left: 60
    },
    x: function(d) {
      return d.x;
    },
    y: function(d) {
      return d.y;
    },
    useInteractiveGuideline: true
  },
  xAxis: {
    axisLabel: 'Time (ms)'
  },
  yAxis: {
    axisLabel: 'Voltage (v)',
    tickFormat: function(d) {
      return d3.format('.2f')(d);
    },
    axisLabelDistance: 0
  }
};
function sinAndCos() {
  var sin = []
    , sin2 = []
    , cos = [];
  for (var i = 0; i < 100; i++) {
    sin.push({
      x: i,
      y: Math.sin(i / 10)
    });
    sin2.push({
      x: i,
      y: i % 10 === 5 ? null : Math.sin(i / 10) * 0.25 + 0.5
    });
    cos.push({
      x: i,
      y: 0.5 * Math.cos(i / 10 + 2) + Math.random() / 10
    });
  }
  return [{
    values: sin,
    key: 'Sine Wave',
    color: COLORS.primary
  }, {
    values: cos,
    key: 'Cosine Wave',
    color: COLORS.danger
  }, {
    values: sin2,
    key: 'Another sine wave',
    color: COLORS.info
  }];
}
var data1 = sinAndCos();
var options2 = {
  chart: {
    height: 150,
    margin: {
      top: 20,
      right: 20,
      bottom: 60,
      left: 55
    },
    x: function(d) {
      return d.label;
    },
    y: function(d) {
      return d.value;
    },
    showValues: true,
    valueFormat: function(d) {
      return d3.format(',.4f')(d);
    },
    transitionDuration: 500
  },
  xAxis: {
    axisLabel: 'X Axis'
  },
  yAxis: {
    axisLabel: 'Y Axis',
    axisLabelDistance: 30
  }
};
var data2 = [{
  key: 'Cumulative Return',
  values: [{
    'label': 'A',
    'value': -29.765957771107
  }, {
    'label': 'B',
    'value': 0
  }, {
    'label': 'C',
    'value': 32.807804682612
  }, {
    'label': 'D',
    'value': 196.45946739256
  }, {
    'label': 'E',
    'value': 0.19434030906893
  }, {
    'label': 'F',
    'value': -98.079782601442
  }, {
    'label': 'G',
    'value': -13.925743130903
  }, {
    'label': 'H',
    'value': -5.1387322875705
  }]
}];
var options3 = {
  chart: {
    height: 150,
    x: function(d) {
      return d.label;
    },
    y: function(d) {
      return d.value;
    },
    showControls: true,
    showValues: true,
    transitionDuration: 500
  },
  xAxis: {
    showMaxMin: false
  },
  yAxis: {
    axisLabel: 'Values',
    tickFormat: function(d) {
      return d3.format(',.2f')(d);
    }
  }
};
var data3 = [{
  'key': 'Series1',
  'color': '#d62728',
  'values': [{
    'label': 'Group A',
    'value': -1.8746444827653
  }, {
    'label': 'Group B',
    'value': -8.0961543492239
  }, {
    'label': 'Group C',
    'value': -0.57072943117674
  }, {
    'label': 'Group D',
    'value': -2.4174010336624
  }, {
    'label': 'Group E',
    'value': -0.72009071426284
  }, {
    'label': 'Group F',
    'value': -0.77154485523777
  }, {
    'label': 'Group G',
    'value': -0.90152097798131
  }, {
    'label': 'Group H',
    'value': -0.91445417330854
  }, {
    'label': 'Group I',
    'value': -0.055746319141851
  }]
}, {
  'key': 'Series2',
  'color': '#1f77b4',
  'values': [{
    'label': 'Group A',
    'value': 25.307646510375
  }, {
    'label': 'Group B',
    'value': 16.756779544553
  }, {
    'label': 'Group C',
    'value': 18.451534877007
  }, {
    'label': 'Group D',
    'value': 8.6142352811805
  }, {
    'label': 'Group E',
    'value': 7.8082472075876
  }, {
    'label': 'Group F',
    'value': 5.259101026956
  }, {
    'label': 'Group G',
    'value': 0.30947953487127
  }, {
    'label': 'Group H',
    'value': 0
  }, {
    'label': 'Group I',
    'value': 0
  }]
}];

class Nvd3Chart extends Component {
  componentDidMount() {
    nv.addGraph(function() {
      var chart = nv.models.lineChart();
      chart.xAxis.options(options1.xAxis);
      chart.yAxis.options(options1.yAxis);
      chart.options(options1.chart);
      d3.select("#nvd3_chart1").datum(data1).call(chart);
      nv.utils.windowResize(chart.update);
      return chart;
    });
    nv.addGraph(function() {
      var chart = nv.models.discreteBarChart();
      chart.xAxis.options(options2.xAxis);
      chart.yAxis.options(options2.yAxis);
      chart.options(options2.chart);
      d3.select("#nvd3_chart2").datum(data2).call(chart);
      nv.utils.windowResize(chart.update);
      return chart;
    });
    nv.addGraph(function() {
      var chart = nv.models.multiBarHorizontalChart();
      chart.xAxis.options(options3.xAxis);
      chart.yAxis.options(options3.yAxis);
      chart.options(options3.chart);
      d3.select("#nvd3_chart3").datum(data3).call(chart);
      nv.utils.windowResize(chart.update);
      return chart;
    });
  }
  componentWillUnmount() {
    $('#nvd3_chart1').empty();
    $('#nvd3_chart2').empty();
    $('#nvd3_chart3').empty();
  }
  render() {
    return (
      <div>
        <div className="row mb25">
          <div className="col-xs-12">
            <div className="panel">
              <div className="panel-heading border"> Line chart </div>
              <div className="panel-body">
                <div className="line-chart">
                  <svg id='nvd3_chart1'></svg>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mb25">
          <div className="col-xs-12">
            <div className="panel">
              <div className="panel-heading border"> DiscreteBar chart </div>
              <div className="panel-body">
                <svg id='nvd3_chart2'></svg>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <div className="panel">
              <div className="panel-heading border"> Multibar horizontal chart </div>
              <div className="panel-body">
                <svg id='nvd3_chart3'></svg>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Nvd3Chart;
