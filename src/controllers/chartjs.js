import React, { Component } from 'react';
import $ from 'jquery';

var BarChart = require("react-chartjs").Bar;
var LineChart = require("react-chartjs").Line;
var PolarAreaChart = require("react-chartjs").PolarArea;
var RadarChart = require("react-chartjs").Radar;
var DoughnutChart = require("react-chartjs").Doughnut;
var PieChart = require("react-chartjs").Pie;

var COLORS = {
  'default': '#e2e2e2',
  'primary': '#09c',
  'success': '#2ECC71',
  'warning': '#ffc65d',
  'danger': '#d96557',
  'info': '#4cc3d9',
  'white': 'white',
  'dark': '#4C5064',
  'border': '#e4e4e4',
  'bodyBg': '#e0e8f2',
  'textColor': '#6B6B6B'
};

var getRandomArbitrary = function() {
  return Math.round(100 * Math.random())
}

function LightenDarkenColor(a, b) {
    var c = !1;
    "#" === a[0] && (a = a.slice(1),
    c = !0);
    var d = parseInt(a, 16)
      , e = (d >> 16) + b;
    e > 255 ? e = 255 : 0 > e && (e = 0);
    var f = (d >> 8 & 255) + b;
    f > 255 ? f = 255 : 0 > f && (f = 0);
    var g = (255 & d) + b;
    return g > 255 ? g = 255 : 0 > g && (g = 0),
    (c ? "#" : "") + (g | f << 8 | e << 16).toString(16)
}

var globalOptions = {
  scaleFontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  scaleFontSize: 10,
  responsive: true,
  tooltipFontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  tooltipFontSize: 12,
  tooltipTitleFontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  tooltipTitleFontSize: 13,
  tooltipTitleFontStyle: '700',
  tooltipCornerRadius: 2,
};
var barOptions = {
  scaleShowGridLines: false,
  barShowStroke: false,
};
$.extend(barOptions, globalOptions);
var barData = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [{
    fillColor: 'rgba(220,220,220,1)',
    highlightFill: 'rgba(220,220,220,1)',
    data: [getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary()]
  }, {
    fillColor: 'rgba(151,187,205,1)',
    highlightFill: 'rgba(151,187,205,1)',
    data: [getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary()]
  }]
};
var lineOptions = {
  scaleShowGridLines: false,
  bezierCurve: false,
  pointDotRadius: 2,
  datasetStrokeWidth: 1,
};
$.extend(lineOptions, globalOptions);
var lineData = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [{
    label: 'My First dataset',
    fillColor: 'rgba(220,220,220,0.2)',
    strokeColor: 'rgba(220,220,220,1)',
    pointColor: 'rgba(220,220,220,1)',
    pointStrokeColor: '#fff',
    pointHighlightFill: '#fff',
    pointHighlightStroke: 'rgba(220,220,220,1)',
    data: [getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary()]
  }, {
    label: 'My Second dataset',
    fillColor: 'rgba(151,187,205,0.2)',
    strokeColor: 'rgba(151,187,205,1)',
    pointColor: 'rgba(151,187,205,1)',
    pointStrokeColor: '#fff',
    pointHighlightFill: '#fff',
    pointHighlightStroke: 'rgba(151,187,205,1)',
    data: [getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary(), getRandomArbitrary()]
  }]
};
var polarOptions = {
  segmentShowStroke: false,
  scaleBackdropColor: 'rgba(255,255,255,1)',
  scaleShowLine: false,
};
$.extend(polarOptions, globalOptions);
var polarData = [{
  value: 80,
  color: COLORS.danger
}, {
  value: 70,
  color: COLORS.info
}, {
  value: 100,
  color: COLORS.warning
}, {
  value: 40,
  color: COLORS.bodyBg
}, {
  value: 120,
  color: COLORS.dark
}, {
  value: 90,
  color: COLORS.primary
}];
var radarOptions = {
  pointDotRadius: 0,
  pointLabelFontFamily: '"Roboto"',
  pointLabelFontSize: 10
};
$.extend(radarOptions, globalOptions);
var radarData = {
  labels: ['Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Partying', 'Running'],
  datasets: [{
    fillColor: 'rgba(220,220,220,1)',
    strokeColor: 'rgba(220,220,220,1)',
    pointColor: 'rgba(220,220,220,1)',
    pointStrokeColor: '#fff',
    data: [65, 59, 90, 81, 56, 55, 40]
  }, {
    fillColor: 'rgba(151,187,205,1)',
    strokeColor: 'rgba(151,187,205,1)',
    pointColor: 'rgba(151,187,205,1)',
    pointStrokeColor: '#fff',
    data: [28, 48, 40, 19, 96, 27, 100]
  }]
};
var doughnutOptions = {
  percentageInnerCutout: 60,
};
$.extend(doughnutOptions, globalOptions);
var doughnutData = [{
  value: 280,
  color: COLORS.danger,
  highlight: LightenDarkenColor(COLORS.danger, 20),
  label: 'Danger'
}, {
  value: 70,
  color: COLORS.success,
  highlight: LightenDarkenColor(COLORS.success, 20),
  label: 'Success'
}, {
  value: 100,
  color: COLORS.warning,
  highlight: LightenDarkenColor(COLORS.warning, 20),
  label: 'Warning'
}, {
  value: 40,
  color: COLORS.bodyBg,
  highlight: LightenDarkenColor(COLORS.bodyBg, 20),
  label: 'Body'
}, {
  value: 120,
  color: COLORS.dark,
  highlight: LightenDarkenColor(COLORS.dark, 20),
  label: 'Dark'
}];
var pieOptions = {
  segmentShowStroke: false
};
$.extend(pieOptions, globalOptions);
var pieData = [{
  value: 300,
  color: COLORS.danger,
  highlight: LightenDarkenColor(COLORS.danger, 20),
  label: 'Danger'
}, {
  value: 50,
  color: COLORS.success,
  highlight: LightenDarkenColor(COLORS.success, 20),
  label: 'Success'
}, {
  value: 100,
  color: COLORS.warning,
  highlight: LightenDarkenColor(COLORS.warning, 20),
  label: 'Warning'
}, {
  value: 40,
  color: COLORS.bodyBg,
  highlight: LightenDarkenColor(COLORS.bodyBg, 20),
  label: 'Body'
}, {
  value: 120,
  color: COLORS.dark,
  highlight: LightenDarkenColor(COLORS.dark, 20),
  label: 'Dark'
}];

class ChartJSChart extends Component {
  render() {
    return (
      <div>
        <div className="row mb25">
          <div className="col-md-12">
            <div className="panel">
              <div className="panel-heading border"> Bar chart </div>
              <div className="panel-body">
                <div className="canvas-holder">
                  <BarChart data={barData} options={barOptions}/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mb25">
          <div className="col-md-12">
            <div className="panel">
              <div className="panel-heading border"> Line chart </div>
              <div className="panel-body">
                <div className="canvas-holder">
                  <LineChart data={lineData} options={lineOptions}/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6 mb25">
            <div className="panel">
              <div className="panel-heading border"> Area chart </div>
              <div className="panel-body">
                <div className="canvas-holder">
                  <PolarAreaChart data={polarData} options={polarOptions}/>
                </div>
              </div>
            </div>
          </div>
          <div className="col-sm-6 mb25">
            <div className="panel">
              <div className="panel-heading border"> Radar chart </div>
              <div className="panel-body">
                <div className="canvas-holder">
                  <RadarChart data={radarData} options={radarOptions}/>
                </div>
              </div>
            </div>
          </div>
          <div className="col-sm-6 mb25">
            <div className="panel">
              <div className="panel-heading border"> Donut chart </div>
              <div className="panel-body">
                <div className="canvas-holder">
                  <DoughnutChart data={doughnutData} options={doughnutOptions}/>
                </div>
              </div>
            </div>
          </div>
          <div className="col-sm-6 mb25">
            <div className="panel">
              <div className="panel-heading border"> Polar chart </div>
              <div className="panel-body">
                <div className="canvas-holder">
                  <PieChart data={pieData} options={pieOptions}/>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ChartJSChart;