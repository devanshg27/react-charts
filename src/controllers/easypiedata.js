import $ from 'jquery';

var EasyPieData = {
  percent1: 86,
  options1: {
    lineWidth: 8,
    barColor: 'rgba(255,255,255,.7)',
    trackColor: 'rgba(0,0,0,.1)',
    lineCap: 'butt',
    easing: 'easeOutBounce',
    onStep: function(from, to, percent) {
      $('#easypiechart1').find('.percent').text(Math.round(percent));
    }
  },
  percent2: 52,
  options2: {
    lineWidth: 8,
    barColor: 'rgba(255,255,255,.7)',
    trackColor: 'rgba(0,0,0,.1)',
    lineCap: 'butt',
    easing: 'easeOutBounce',
    onStep: function(from, to, percent) {
      $('#easypiechart2').find('.percent').text(Math.round(percent));
    }
  },
  percent3: 76,
  options3: {
    lineWidth: 15,
    barColor: 'rgba(255,255,255,.7)',
    trackColor: false,
    lineCap: 'round',
    easing: 'easeOutBounce',
    onStep: function(from, to, percent) {
      $('#easypiechart3').find('.percent').text(Math.round(percent));
    }
  },
  percent4: 82,
  options4: {
    lineWidth: 15,
    barColor: 'rgba(255,255,255,.7)',
    trackColor: 'rgba(0,0,0,.1)',
    lineCap: 'butt',
    easing: 'easeOutBounce',
    onStep: function(from, to, percent) {
      $('#easypiechart4').find('.percent').text(Math.round(percent));
    }
  },
  percent5: 54,
  options5: {
    lineWidth: 8,
    barColor: 'rgba(255,255,255,.7)',
    trackColor: 'rgba(0,0,0,.1)',
    lineCap: 'round',
    easing: 'easeOutBounce',
    scaleColor: false,
    onStep: function(from, to, percent) {
      $('#easypiechart5').find('.percent').text(Math.round(percent));
    }
  },
  percent6: 43,
  options6: {
    lineWidth: 2,
    barColor: 'rgba(255,255,255,.7)',
    trackColor: 'rgba(0,0,0,.1)',
    lineCap: 'round',
    easing: 'easeOutBounce',
    scaleColor: false,
    onStep: function(from, to, percent) {
      $('#easypiechart6').find('.percent').text(Math.round(percent));
    }
  },
  percent7: 43,
  options7: {
    lineWidth: 14,
    barColor: 'rgba(255,255,255,.7)',
    trackColor: 'rgba(0,0,0,.1)',
    lineCap: 'butt',
    easing: 'easeOutBounce',
    scaleColor: false,
    onStep: function(from, to, percent) {
      $('#easypiechart7').find('.percent').text(Math.round(percent));
    }
  },
}

export default EasyPieData;
