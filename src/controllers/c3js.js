import React, { Component } from 'react';
import C3Chart from 'react-c3js';
import 'c3/c3.css';

import c3jsdata from './c3jsdata';

class C3JSChart extends Component {
  render() {
    return (
      <div className="row">
        <div className="col-xs-12">
          <div className="panel">
            <div className="panel-heading border"> C3 chart demo </div>
            <div className="panel-body">
              <div className="c3chart">
                <C3Chart data={c3jsdata.chart} />
              </div>
            </div>
          </div>
        </div>
        <div className="col-xs-12">
          <div className="panel">
            <div className="panel-heading border"> C3 chart demo </div>
            <div className="panel-body">
              <C3Chart data={c3jsdata.chart2} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default C3JSChart;
