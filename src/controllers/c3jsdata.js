var C3JSData = {
  chart: {
    columns: [
      ['Data1', 30, 200, 100, 400, 150, 250, 50, 100, 250],
      ['Data2', 100, 30, 200, 320, 50, 150, 230, 80, 150]
    ],
    type: 'line',
    selection: {
      enabled: true
    },
  },
  chart2: {
    columns: [
      ['Data1', 30, 200, 100, 400, 150, 250, 50, 100, 250],
      ['Data2', 100, 30, 200, 320, 50, 150, 230, 80, 150]
    ],
    type: 'bar',
    selection: {
      enabled: true
    },
  }
}

export default C3JSData;
