import React, { Component } from 'react';
import $ from 'jquery';
import EasyPieChart from 'easy-pie-chart';
import easypiedata from './easypiedata';

class EasypieChart extends Component {
  componentDidMount() {
    var element1 = document.querySelector('#easypiechart1');
    new EasyPieChart(element1, easypiedata.options1);
    var element2 = document.querySelector('#easypiechart2');
    new EasyPieChart(element2, easypiedata.options2);
    var element3 = document.querySelector('#easypiechart3');
    new EasyPieChart(element3, easypiedata.options3);
    var element4 = document.querySelector('#easypiechart4');
    new EasyPieChart(element4, easypiedata.options4);
    var element5 = document.querySelector('#easypiechart5');
    new EasyPieChart(element5, easypiedata.options5);
    var element6 = document.querySelector('#easypiechart6');
    new EasyPieChart(element6, easypiedata.options6);
  }
  componentWillUnmount() {
    $('#easypiechart1').html("<div><div class=\"percent h1\"></div><small>Bounce Rate</small> </div>");
    $('#easypiechart2').html("<div><div class=\"percent h1\"></div><small>New user signup Rate</small> </div>");
    $('#easypiechart3').html("<div><div class=\"percent h1\"></div><small>Daily visitors</small> </div>");
    $('#easypiechart4').html("<div><div class=\"percent h1\"></div><small>Percentage new visitors</small> </div>");
    $('#easypiechart5').html("<div><div class=\"percent h1\"></div><small>New servers nodes created</small> </div>");
    $('#easypiechart6').html("<div><div class=\"percent h1\"></div><small>Junk files purged</small> </div>");
  }
  render() {
    return (
      <div className="row">
        <div className="col-md-6 col-lg-4">
          <div className="panel bg-danger" style={{color: '#fff', backgroundColor: '#d96557'}}>
            <div className="panel-heading border"> Bounce rate </div>
            <div className="panel-body text-center">
              <div className="piechart m25">
                <div className="bounce_pie" id='easypiechart1' data-percent={easypiedata.percent1}>
                  <div>
                    <div className="percent h1"></div>
                    <small>Bounce Rate</small> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6 col-lg-4">
          <div className="panel bg-success" style={{color: '#fff', backgroundColor: '#2ecc71'}}>
            <div className="panel-heading border"> Signup rate </div>
            <div className="panel-body text-center">
              <div className="piechart m25">
                <div className="total" id='easypiechart2' data-percent={easypiedata.percent2}>
                  <div>
                    <div className="percent h1"></div>
                    <small>New user signup Rate</small> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6 col-lg-4">
          <div className="panel bg-info" style={{color: '#fff', backgroundColor: '#4cc3d9'}}>
            <div className="panel-heading border"> Visitors </div>
            <div className="panel-body text-center">
              <div className="piechart m25">
                <div className="dailyvisitors" id='easypiechart3' data-percent={easypiedata.percent3}>
                  <div>
                    <div className="percent h1"></div>
                    <small>Daily visitors</small> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6 col-lg-4">
          <div className="panel bg-warning" style={{color: '#fff', backgroundColor: '#ffc65d'}}>
            <div className="panel-heading border"> New Visitors </div>
            <div className="panel-body text-center">
              <div className="piechart m25">
                <div className="newvisitors" id='easypiechart4' data-percent={easypiedata.percent4}>
                  <div>
                    <div className="percent h1"></div>
                    <small>Percentage new visitors</small> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6 col-lg-4">
          <div className="panel bg-primary" style={{color: '#fff', backgroundColor: '#09c'}}>
            <div className="panel-heading border"> Server Nodes </div>
            <div className="panel-body text-center">
              <div className="piechart m25">
                <div className="servernodes" id='easypiechart5' data-percent={easypiedata.percent5}>
                  <div>
                    <div className="percent h1"></div>
                    <small>New servers nodes created</small> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6 col-lg-4">
          <div className="panel bg-blue" style={{color: '#fff', backgroundColor: '#2196f3'}}>
            <div className="panel-heading border"> junk Files </div>
            <div className="panel-body text-center">
              <div className="piechart m25">
                <div className="junkfiles" id='easypiechart6' data-percent={easypiedata.percent6}>
                  <div>
                    <div className="percent h1"></div>
                    <small>Junk files purged</small> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EasypieChart;
